.. FEniCS Containers documentation master file, created by
   sphinx-quickstart on Sun Mar 13 10:22:20 2016.  You can adapt this
   file completely to your liking, but it should at least contain the
   root `toctree` directive.

================
FEniCS in Docker
================

An easy way to run `FEniCS <http://fenicsproject.org>`_ is to use our
prebuilt, high-performance Docker images.  This documentation explains
how to get quickly started with using FEniCS in Docker, as well as how
to take advantage of more advanced features of Docker.

.. note:

   This documentation is under development.


.. toctree::
   :numbered:
   :maxdepth: 2

   quickstart
   introduction
   images
   work_flows
   jupyter
   performance
   developing
   developer_notes
   troubleshooting

Contact
=======

Support requests can be sent to the FEniCS Support mailing list
(fenics-support@googlegroups.com).

For development-related questions and suggestions, use the FEniCS
Development mailing list (fenics-dev@googlegroups.com). Bugs can be
registered on the Bitbucket Issue Tracker
(https://bitbucket.org/fenics-project/docker/issues).


Background
==========

Background on the use of containers with FEniCS, including performance
data, is available in the paper

  Hale, J. S., Li, L., Richardson, C. N., and Wells, G. N. Containers
  for portable, productive and performant scientific computing. To
  appear in *IEEE Computing and Science and Engineering*.
  [`arXiv:1608.07573 <http://arxiv.org/abs/1608.07573>`_]


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
